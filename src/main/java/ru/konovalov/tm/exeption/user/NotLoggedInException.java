package ru.konovalov.tm.exeption.user;

import ru.konovalov.tm.exeption.AbstractException;

public class NotLoggedInException extends AbstractException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}
