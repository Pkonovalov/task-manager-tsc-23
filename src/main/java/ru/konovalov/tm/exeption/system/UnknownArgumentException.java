package ru.konovalov.tm.exeption.system;

import ru.konovalov.tm.exeption.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not found...");
    }

}
