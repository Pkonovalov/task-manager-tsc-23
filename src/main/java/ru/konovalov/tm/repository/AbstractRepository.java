package ru.konovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        list.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        list.remove(entity);
    }

    @Override
    public int size() {
        return list.size();
    }

    public E removeById(@NotNull final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return null;
    }

    public E findById (@NotNull final String id){
            for (E entity : list) {
                if (id.equals(entity.getId())) return entity;
            }
            return null;
        }
}



