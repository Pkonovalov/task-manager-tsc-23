package ru.konovalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

    int size();

    void add(@Nullable E entity);

    @Nullable
    E findById(@NotNull String id);

    void remove(@Nullable E entity);

    @Nullable
    E removeById(@NotNull String id);

}
