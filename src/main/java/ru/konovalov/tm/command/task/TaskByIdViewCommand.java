package ru.konovalov.tm.command.task;

import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

public final class TaskByIdViewCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-view-by-id";
    }

    @Override
    public String description() {
        return "View task by id";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        showTask(serviceLocator.getTaskService().findOneById(user.getId(), TerminalUtil.nextLine()));
        System.out.println("[OK]");
    }

}
