package ru.konovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.api.IService;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public void add(@NotNull final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Nullable
    public E findById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    public E removeById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
        return null;
    }


}
