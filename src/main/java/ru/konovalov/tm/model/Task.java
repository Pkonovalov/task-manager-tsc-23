package ru.konovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.entity.ITWBS;
import ru.konovalov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractOwner implements ITWBS {

    @Nullable
    private String projectId;

    @Nullable
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date created;

    public Task(@Nullable String name) {
        this.name = name;
    }

    public Task(@Nullable String name,@Nullable String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
